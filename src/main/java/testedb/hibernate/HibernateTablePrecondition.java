package testedb.hibernate;

import static testedb.hibernate.ConfigVerifier.verifyClassIsATable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;


public class HibernateTablePrecondition {
	private final HibernateTestDB testDB;
	
	private HibernateTablePrecondition(HibernateTestDB testDB) { 
		this.testDB = testDB;
	}
	
	public static final HibernateTablePrecondition givenThat(final HibernateTestDB testDB) {
		return new HibernateTablePrecondition(testDB);
	}
	
	public <PersistentType> testedb.PreconditionEnforcer<PersistentType> table(Class<PersistentType> tableClass) {
		verifyClassIsATable(tableClass);
		return new PreconditionEnforcer<PersistentType>(tableClass);
	}
	
	public class PreconditionEnforcer<T> extends testedb.PreconditionEnforcer<T>{
		private final Class<T> tableClass;
		
		PreconditionEnforcer(Class<T> tableClass) { 
			this.tableClass = tableClass;
		}
		
		public void contains(T... objectsToAdd) {
			SessionFactory sessionFactory = testDB.getSessionFactory();
			Session session = sessionFactory.getCurrentSession();
			
			for (T objectToAdd : objectsToAdd) {
				session.saveOrUpdate(objectToAdd);
			}
			
			session.flush();
		}
		
		public void isEmptyTable() {
			SessionFactory sessionFactory = testDB.getSessionFactory();
			Session session = sessionFactory.getCurrentSession();
			
			String sql = new StringBuffer()
				.append(" DELETE FROM ").append(tableClass.getName())
				.toString();
			
			session.createQuery(sql).executeUpdate();
		}
	}
}
