package testedb.hibernate;

import java.lang.annotation.Annotation;

public class ConfigVerifier {
	
	public static <T> void verifyClassIsATable(Class<T> classOfTable) {
		if (!containsAnnotation(javax.persistence.Table.class, classOfTable.getAnnotations())) {
			throw new TableAnnotationMissingException(classOfTable);
		}
	}
	
	private static boolean containsAnnotation(Class requiredAnnotation, Annotation[] annotations) {
		for (Annotation annotation : annotations) {
			if(annotation.annotationType().equals(requiredAnnotation)) {
				return true;
			}
		}
		
		return false;
	}

	public static class TableAnnotationMissingException extends RuntimeException {
		private static final long serialVersionUID = 3362408465777426855L;

		public TableAnnotationMissingException(Class tableClass) {
			super(tableClass + " does not contain the javax.persistence.Table annotation!");
		}
	}
}