package testedb.hibernate;

import static org.junit.Assert.*;
import static testedb.hibernate.ConfigVerifier.verifyClassIsATable;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;


public class HibernateTablePostcondition {
	private final HibernateTestDB testDB;
	
	HibernateTablePostcondition(HibernateTestDB testDB) {
		this.testDB = testDB;
	}
	
	public static HibernateTablePostcondition assertThat(HibernateTestDB testDB) {		
		return new HibernateTablePostcondition(testDB);
	}
	
	public <PersistentType> testedb.PostconditionVerifier<PersistentType> table(Class<PersistentType> tableClass) {
		verifyClassIsATable(tableClass);
		return new PostconditionVerifier<PersistentType>(tableClass);
	}
	
	public class PostconditionVerifier<T> extends testedb.PostconditionVerifier<T> {
		private final Class<T> tableClass;
		
		PostconditionVerifier(Class<T> tableClass) {
			this.tableClass = tableClass;
		}

		public void contains(T... requiredItems) {
			List<T> itemsIntersectingRequiredItems = intersection(requiredItems);

			for (T item : requiredItems) {
				assertTrue("TestDB contains " + item + " in table " + tableClass.getName(), 
						itemsIntersectingRequiredItems.contains(item));
			}
		}
		
		public void containsOnly(T... onlyItems) {
			List<T> itemsIntersectingSelectedItems = intersection(onlyItems);
			List<T> complementOfOnlyItems = complementOf(onlyItems);
			
			for (T item : onlyItems) {
				assertTrue("TestDB should contain " + item + " in table " + tableClass.getName(), 
						itemsIntersectingSelectedItems.contains(item));
			}
			
			assertTrue("TestDB contains unexpected items: " + complementOfOnlyItems, 
					complementOfOnlyItems.isEmpty());
		}

		public void doesNotContain(T... prohibitedItems) {
			List<T> itemsIntersectingProhibitedItems = intersection(prohibitedItems);
			
			for (T item : prohibitedItems) {
				assertFalse("testDB should not contain " + item + " in table " + tableClass.getName(), 
						itemsIntersectingProhibitedItems.contains(item));
			}
		}
		
		private List<T> intersection(T... interestingItems) {
			SessionFactory sessionFactory = testDB.getSessionFactory();
			Session session = sessionFactory.getCurrentSession();
			session.flush();
			
			String sql = new StringBuffer()
				.append(" SELECT table ")
				.append(" FROM ").append(tableClass.getName()).append(" as table ")
				.append(" WHERE table in (:interestingItems)")
				.toString();
			
			Query query = session.createQuery(sql.toString());
			query.setParameterList("interestingItems", interestingItems);
			
			@SuppressWarnings("unchecked")
			List<T> itemsIntersectingInterestingItems = query.list();
			return itemsIntersectingInterestingItems;
		}
		
		private List<T> complementOf(T... interestingItems) {
			SessionFactory sessionFactory = testDB.getSessionFactory();
			Session session = sessionFactory.getCurrentSession();
			session.flush();
			
			String sql = new StringBuffer()
				.append(" SELECT table ")
				.append(" FROM ").append(tableClass.getName()).append(" as table ")
				.append(" WHERE table not in (:interestingItems)")
				.toString();
			
			Query query = session.createQuery(sql.toString());
			query.setParameterList("interestingItems", interestingItems);
			
			@SuppressWarnings("unchecked")
			List<T> itemsIntersectingInterestingItems = query.list();
			return itemsIntersectingInterestingItems;
		}
	}
}
