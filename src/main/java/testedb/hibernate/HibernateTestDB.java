package testedb.hibernate;

import org.hibernate.SessionFactory;

public class HibernateTestDB {
	// TODO should actually create the session factory and not just wire it in
	private final SessionFactory sessionFactory;
	
	public HibernateTestDB(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}
}
