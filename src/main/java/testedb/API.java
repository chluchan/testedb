package testedb;

import testedb.hibernate.HibernateTablePostcondition;
import testedb.hibernate.HibernateTablePrecondition;
import testedb.hibernate.HibernateTestDB;
import testedb.jdbc.Fields;
import testedb.jdbc.JDBCTablePostcondition;
import testedb.jdbc.JDBCTablePrecondition;
import testedb.jdbc.JDBCTestDB;
import testedb.jdbc.RowOfValues;

public class API {
	private API() {}
	
	/** 
	 * declare what the state of a table in the database should be.
	 * The database will be altered to match this state.<br><br>
	 * 
	 * example:<br>
	 * givenThat(testDB).table("person", fields("id", "name").containsOnly(row(1, "bob"), row(2, "joe"));
	 * 
	 * @param testDB
	 * @return JDBCTablePrecondition
	 */
	public static JDBCTablePrecondition givenThat(JDBCTestDB testDB) {
		return JDBCTablePrecondition.givenThat(testDB);
	}
	
	/**
	 * Assert something about the state of a database table.
	 * If the database does not match this state it will fail a junit test.<br><br>
	 * 
	 * example:<br>
	 * assertThat(testDB).table("person", fields("id", "name")).contains(row(3, "sue"));
	 * 
	 * @param testDB
	 * @return HibernateTablePostcondition
	 */
	public static JDBCTablePostcondition assertThat(JDBCTestDB testDB) {
		return JDBCTablePostcondition.assertThat(testDB);
	}
	
	/**
	 * lists the fields used with a table for either a given or assert.<br>
	 * For example, a table named "person" with columns "id", "name", and "address"
	 * could be described as:<BR><BR>
	 * 
	 * table("person", fields("id", "name", "address"))
	 * 
	 * @param fields
	 * @return
	 */
	public static Fields fields(String... fields) {
		return Fields.fields(fields);
	}

	/**
	 * describes a row of values in a table.<br>
	 * for example, table("person", fields("id", "name")) might contain:<br><br>
	 * 
	 * row(5, "Bob"), row(12, "Jim"), row(23, "Robert")
	 * 
	 * @param values
	 * @return
	 */
	public static RowOfValues row(Object... values) {
		return RowOfValues.row(values);
	}
	
	/**
	 * declare what the state of a table in the database should be.
	 * The database will be altered to match this state.<br><br>
	 * 
	 * example:<br>
	 * givenThat(testDB).table(Person.class).containsOnly(bob, joe);
	 * 
	 * @param testDB
	 * @return HibernateTablePrecondition
	 */
	public static final HibernateTablePrecondition givenThat(final HibernateTestDB testDB) {
		return HibernateTablePrecondition.givenThat(testDB);
	}
	
	/**
	 * Assert something about the state of a database table.
	 * If the database does not match this state it will fail a junit test.<br><br>
	 * 
	 * example:<br>
	 * assertThat(testDB).table(Person.class).contains(theNewGuy);
	 * 
	 * @param testDB
	 * @return HibernateTablePostcondition
	 */
	public static HibernateTablePostcondition assertThat(HibernateTestDB testDB) {		
		return HibernateTablePostcondition.assertThat(testDB);
	}
}
