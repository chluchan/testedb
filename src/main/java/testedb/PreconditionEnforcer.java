package testedb;

public abstract class PreconditionEnforcer<ROW> {

	public abstract void contains(ROW... rows);
	
	public abstract void isEmptyTable();
	
	public void containsOnly(ROW... allRows) {
		isEmptyTable();
		contains(allRows);
	}
	
}
