package testedb;

public abstract class PostconditionVerifier<ROW> {
	
	public abstract void contains(ROW... someRows);
	
	public abstract void containsOnly(ROW... allRows);
	
	public abstract void doesNotContain(ROW... prohibitedRows);
	
}
