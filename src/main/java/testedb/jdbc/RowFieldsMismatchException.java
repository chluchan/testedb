package testedb.jdbc;

public class RowFieldsMismatchException extends RuntimeException {
	private static final long serialVersionUID = 2560812118347721611L;

	public RowFieldsMismatchException(Fields fields, RowOfValues row) {
		super(row + " does not match fields" + fields.toString() + "" );
	}
}