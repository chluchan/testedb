package testedb.jdbc;

import javax.sql.DataSource;

public class JDBCTestDB {
	
	private final DataSource dataSource;
	
	public JDBCTestDB(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	public DataSource dataSource() {
		return dataSource;
	}
}
