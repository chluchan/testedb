package testedb.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class JDBCTablePrecondition {
	
	private final JDBCTestDB testDB;
	
	private JDBCTablePrecondition(JDBCTestDB testDB) { 
		this.testDB = testDB;
	}

	public static JDBCTablePrecondition givenThat(JDBCTestDB testDB) {
		return new JDBCTablePrecondition(testDB);
	}
	
	public testedb.PreconditionEnforcer<RowOfValues> table(String tableName, Fields fields) {
		return new PreconditionEnforcer(tableName, fields);		
	}
	
	public class PreconditionEnforcer extends testedb.PreconditionEnforcer<RowOfValues> {
		
		private final String tableName;
		private final Fields fields;
		
		PreconditionEnforcer(String tableName, Fields fields) {
			this.tableName = tableName;
			this.fields = fields;
		}

		@Override
		public void contains(RowOfValues... rows) {
			if (fields.names().length == 0 && rows.length > 0) {
				throw new IllegalArgumentException(tableName + " should have some fields ");
			}
			
			try {
				
				String sql = " INSERT INTO " + tableName + " (" + fields.toCommaDelimitedString() + ") " + 
						     " VALUES " + fields.toMaskedString();
				
				Connection connection = testDB.dataSource().getConnection();
				PreparedStatement insert = connection.prepareStatement(sql);
				
				int rowsInBatch = 0;
				for (RowOfValues row : rows) {
					if (row.values().length != fields.names().length) {
						throw new RowFieldsMismatchException(fields, row);
					}
					
					addRowToStatement(row, insert);
					
					insert.addBatch();
					if (rowsInBatch % 500 == 499) {
						insert.executeBatch();
					}
				}
				insert.executeBatch();
		
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}

		@Override
		public void isEmptyTable() {
			
			try {
			
				String sql = " DELETE FROM " + tableName;
				Connection connection = testDB.dataSource().getConnection();
				PreparedStatement delete = connection.prepareStatement(sql);
				delete.executeUpdate();
			
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
			
		}
		
		private void addRowToStatement(RowOfValues row, PreparedStatement statement) throws SQLException {
			for (int i=0; i < row.values().length; i++) {
				statement.setObject(i + 1, row.values()[i]);
			}
		}
		
	}
}
