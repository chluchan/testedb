package testedb.jdbc;

public class Fields {
	
	private final String[] fields;
	
	private Fields(String[] fields) {
		this.fields = fields;
	}
	
	private Fields() {
		throw new IllegalArgumentException("should have values");
	}
	
	public String[] names() {
		return fields;
	}
	
	public String toCommaDelimitedString() {
		if (fields.length == 0) {
			return "";
		}
		
		StringBuilder commaDelimited = new StringBuilder("").append(fields[0]);
		for (int i=1; i < fields.length; i++) {
			commaDelimited.append(", ").append(fields[i]);
		}
		return commaDelimited.toString();
	}
	
	public String toMaskedString() {
		if (fields.length == 0) {
			return "";
		}
		
		StringBuilder commaDelimited = new StringBuilder("(?");
		for (int i=1; i < fields.length; i++) {
			commaDelimited.append(", ?");
		}
		return commaDelimited.append(")").toString();
	}
	
	public static final Fields fields(String... fields) {
		return new Fields(fields);
	}
	
}
