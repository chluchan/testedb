package testedb.jdbc;

public class RowOfValues {

	private final Object[] values;
	
	private RowOfValues(Object[] values) {
		if (values == null) {
			throw new IllegalArgumentException("values cannot be null");
		}
		
		this.values = values;
	}
	
	public Object[] values() {
		return values;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (!(obj instanceof RowOfValues)) {
			return false;
		}
		
		RowOfValues otherValues = (RowOfValues)obj;
		
		if (values.length != otherValues.values().length) {
			return false;
		}
		
		for (int i=0; i < values.length; i++) {
			if (!values[i].equals(otherValues.values()[i])) {
				return false;
			}
		}
		
		return true;
	}
	
	public String toString() {
		if (values.length == 0) {
			return "row()";
		}
		
		StringBuilder commaDelimited = new StringBuilder("row(").append(values[0]);
		for (int i=1; i < values.length; i++) {
			commaDelimited.append(", ").append(values[i]);
		}
		return commaDelimited.append(")").toString();
	}
	
	public int hashCode() {
		int hashCode = 17;
		
		for (int i=0; i < values.length; i++) {
			int valuesHashCode = 37;
			if (values[i] != null) {
				valuesHashCode *= values[i].hashCode();
			}
			hashCode += (i + valuesHashCode);
		}
		
		return hashCode;
	}
	
	public static final RowOfValues row(Object... values) {
		return new RowOfValues(values);
	}
	
}
