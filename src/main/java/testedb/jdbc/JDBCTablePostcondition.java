package testedb.jdbc;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static testedb.API.row;
import static java.util.Arrays.asList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;

public class JDBCTablePostcondition {

	private final JDBCTestDB testDB;
	
	private JDBCTablePostcondition(JDBCTestDB testDB) {
		this.testDB = testDB;
	}
	
	public static JDBCTablePostcondition assertThat(JDBCTestDB testDB) {
		return new JDBCTablePostcondition(testDB);
	}
	
	public testedb.PostconditionVerifier<RowOfValues> table(String tableName, Fields fields) {
		return new PostconditionVerifier(tableName, fields);
	}
	
	public class PostconditionVerifier extends testedb.PostconditionVerifier<RowOfValues> {

		private final String tableName;
		private final Fields fields;
		
		PostconditionVerifier(String tableName, Fields fields) {
			this.tableName = tableName;
			this.fields = fields;
		}
		
		@Override
		public void contains(RowOfValues... requiredItems) {
			Collection<RowOfValues> rowsInTable = rowsFoundInTable();
			for (RowOfValues item : requiredItems) {
				boolean containsIt = rowsInTable.contains(item);
				assertTrue("TestDB contains " + item + " in table " + tableName, 
						rowsInTable.contains(item));
			}
		}

		@Override
		public void containsOnly(RowOfValues... requiredRows) {
			Collection<RowOfValues> rowsInTable = rowsFoundInTable();
			
			for (RowOfValues item : requiredRows) {
				boolean containsIt = rowsInTable.contains(item);
				assertTrue("TestDB contains " + item + " in table " + tableName, 
						rowsInTable.contains(item));
			}
			
			rowsInTable.removeAll(asList(requiredRows));
			assertTrue("TestDB contains unexpected rows: " + rowsInTable, rowsInTable.isEmpty());
		}

		@Override
		public void doesNotContain(RowOfValues... prohibitedRows) {
			Collection<RowOfValues> rowsInTable = rowsFoundInTable();
			for (RowOfValues item : prohibitedRows) {
				assertFalse("TestDB should not contain " + item + " in table " + tableName, 
						rowsInTable.contains(item));
			}
		}
		
		private Collection<RowOfValues> rowsFoundInTable() {	
			PreparedStatement select = null;
			ResultSet results = null;
			
			try {
				String sql = " SELECT " + fields.toCommaDelimitedString() + 
					     	 " FROM " + tableName;
				
				Connection connection = testDB.dataSource().getConnection();
				select = connection.prepareStatement(sql);
				
				Collection<RowOfValues> rowsFoundInTable = new HashSet<RowOfValues>();
				
				int numberOfValuesInEachRow = fields.names().length;
				results = select.executeQuery();			
				while (results.next()) {
					Object[] values = new Object[numberOfValuesInEachRow];
					for (int i=0; i < numberOfValuesInEachRow; i++) {
						values[i] = results.getObject(i+1);
					}
					
					rowsFoundInTable.add(row(values));
				}
				
				return rowsFoundInTable;
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
			finally {
				try {
					if (select != null) {
						select.close();
					}
					
					if (results != null) {
						results.close();
					}
				} catch (SQLException e) {
				}
			}
		}
		
	}
	
}
