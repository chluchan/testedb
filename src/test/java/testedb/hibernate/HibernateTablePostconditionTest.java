package testedb.hibernate;

import static org.junit.Assert.*;
import static testedb.API.givenThat;
import static testedb.API.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import sample.domain.Person;
import sample.model.PersonDAO;
import testedb.hibernate.ConfigVerifier.TableAnnotationMissingException;
import testedb.hibernate.HibernateTablePostcondition.PostconditionVerifier;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:**model-config.xml")
@TransactionConfiguration
@Transactional
public class HibernateTablePostconditionTest {

	@Autowired
	private PersonDAO personDao;
	
	@Autowired
	private HibernateTestDB testDB;
	
	@Test(expected = TableAnnotationMissingException.class)
	public void shouldThrowExceptionIfTheresNoTableAnnotation() {
		assertThat(testDB).table(Object.class);
	}

	@Test
	public void shouldCreatePostconditionVerifier() {
		testedb.PostconditionVerifier<Person> verifier =
				assertThat(testDB).table(Person.class);
	}
	
	@Test
	public void containsShouldPassWhenRequiredItemsAreFound() {
		Person joeBob = joeBob();
		Person johnDoe = johnDoe();
		Person mikeWatson = mikeWatson();
		
		givenThat(testDB).table(Person.class)
			.containsOnly(joeBob, johnDoe, mikeWatson);
		
		assertThat(testDB).table(Person.class)
			.contains(joeBob);
	}
	
	@Test(expected = AssertionError.class)
	public void containsShouldFailWhenRequiredItemsAreNotFound() {
		Person joeBob = joeBob();
		Person johnDoe = johnDoe();
		Person mikeWatson = mikeWatson();
		mikeWatson.setId(500L);
		
		givenThat(testDB).table(Person.class)
			.containsOnly(joeBob, johnDoe);
		
		assertThat(testDB).table(Person.class)
			.contains(mikeWatson, johnDoe);
	}
	
	@Test
	public void doesNotContainShouldPassWhenItemDoesntExist() {
		Person joeBob = joeBob();
		Person johnDoe = johnDoe();
		Person mikeWatson = mikeWatson();
		mikeWatson.setId(500L);
		
		givenThat(testDB).table(Person.class)
			.containsOnly(joeBob, johnDoe);
		
		assertThat(testDB).table(Person.class)
			.doesNotContain(mikeWatson);
	}
	
	@Test(expected = AssertionError.class)
	public void doesNotContainShouldFailWhenItemIsFound() {
		Person joeBob = joeBob();
		Person johnDoe = johnDoe();
		Person mikeWatson = mikeWatson();
		mikeWatson.setId(500L);
		
		givenThat(testDB).table(Person.class)
			.containsOnly(joeBob, johnDoe);
		
		assertThat(testDB).table(Person.class)
			.doesNotContain(johnDoe, mikeWatson);
	}
	
	@Test
	public void containsOnlyPassesWithExactMatch() {
		Person joeBob = joeBob();
		Person johnDoe = johnDoe();
		Person mikeWatson = mikeWatson();
		
		givenThat(testDB).table(Person.class)
			.containsOnly(joeBob, johnDoe, mikeWatson);
		
		assertThat(testDB).table(Person.class)
			.containsOnly(joeBob, johnDoe, mikeWatson);
	}
	
	@Test(expected = AssertionError.class)
	public void containsOnlyFailsWithMissingItem() {
		Person joeBob = joeBob();
		Person johnDoe = johnDoe();
		Person mikeWatson = mikeWatson();
		mikeWatson.setId(500L);
		
		givenThat(testDB).table(Person.class)
			.containsOnly(joeBob, johnDoe);
		
		assertThat(testDB).table(Person.class)
			.containsOnly(joeBob, johnDoe, mikeWatson);
	}
	
	@Test(expected = AssertionError.class)
	public void containsOnlyFailsWithUnexpectedItem() {
		Person joeBob = joeBob();
		Person johnDoe = johnDoe();
		Person mikeWatson = mikeWatson();
		
		givenThat(testDB).table(Person.class)
			.containsOnly(joeBob, johnDoe, mikeWatson);
		
		assertThat(testDB).table(Person.class)
			.containsOnly(joeBob, mikeWatson);
	}
	
	private static final Person joeBob() {
		Person person = new Person();
		person.setName("Joe Bob");
		person.setPhoneNumber(2569375148L);
		person.setAddress("642 Elm Ave");
		return person;
	}
	
	private static final Person johnDoe() {
		Person person = new Person();
		person.setName("John Doe");
		person.setPhoneNumber(2569375148L);
		person.setAddress("642 Elm Ave");
		return person;
	}
	
	private static final Person mikeWatson() {
		Person person = new Person();
		person.setName("Mike Watson");
		person.setPhoneNumber(2523475148L);
		person.setAddress("1545 Commerce Cir");
		return person;
	}
}
