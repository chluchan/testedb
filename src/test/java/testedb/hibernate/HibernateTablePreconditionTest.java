package testedb.hibernate;

import static org.junit.Assert.*;
import static testedb.API.givenThat;
import static java.util.Arrays.asList;

import java.util.List;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import sample.domain.Person;
import sample.model.PersonDAO;
import testedb.hibernate.ConfigVerifier.TableAnnotationMissingException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:**model-config.xml")
@TransactionConfiguration
@Transactional
public class HibernateTablePreconditionTest {

	@Autowired
	private PersonDAO personDao;
	
	@Autowired
	private HibernateTestDB testDB;
	
	@Test(expected = TableAnnotationMissingException.class)
	public void shouldThrowExceptionIfTheresNoTableAnnotation() {
		givenThat(testDB).table(Object.class);
	}
	
	@Test
	public void shouldCreatePreconditionEnforcer() {
		testedb.PreconditionEnforcer<Person> enforcer = 
				givenThat(testDB).table(Person.class);
		
		assertNotNull(enforcer);
	}
	
	@Test
	public void containsShouldAddAllObjects() {		
		givenThat(testDB).table(Person.class)
			.contains(joeBob(), johnDoe());
		
		List<Person> allPeople = personDao.allPeople();
		
		assertTrue(allPeople.containsAll(asList(joeBob(), johnDoe())));
	}
	
	@Test
	public void isEmptyShouldDeleteAllObjects() {
		givenThat(testDB).table(Person.class)
			.contains(joeBob(), johnDoe());
		givenThat(testDB).table(Person.class)
			.isEmptyTable();
		
		assertTrue(personDao.allPeople().isEmpty());
	}
	
	@Test
	public void containsOnlyShouldNotContainAnyPriorEntries() {
		givenThat(testDB).table(Person.class)
			.contains(joeBob());
		givenThat(testDB).table(Person.class)
			.containsOnly(johnDoe());
		
		List<Person> allPeople = personDao.allPeople();
		
		assertEquals(1, allPeople.size());
		assertTrue(allPeople.contains(johnDoe()));
	}
	
	private static final Person joeBob() {
		Person person = new Person();
		person.setName("Joe Bob");
		person.setPhoneNumber(2569375148L);
		person.setAddress("642 Elm Ave");
		return person;
	}
	
	private static final Person johnDoe() {
		Person person = new Person();
		person.setName("John Doe");
		person.setPhoneNumber(2569375148L);
		person.setAddress("642 Elm Ave");
		return person;
	}
}
