package testedb.jdbc;

import static org.junit.Assert.*;
import static testedb.API.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import sample.domain.Person;
import sample.model.PersonDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:**model-config.xml")
@TransactionConfiguration
@Transactional
public class JDBCTablePreconditionTest {
	
	@Autowired
	private PersonDAO personDao;
	
	@Autowired
	private JDBCTestDB testDB;
	
	@Test
	public void shouldCreatePreconditionEnforcer() {
		testedb.PreconditionEnforcer<RowOfValues> preconditionEnforcer =
			givenThat(testDB).table("person", fields("id", "name", "address"));
		
		assertNotNull(preconditionEnforcer);
		assertTrue(preconditionEnforcer instanceof testedb.PreconditionEnforcer);
	}
	
	@Test
	public void containsShouldAddAllValues() {	
		givenThat(testDB).table("person", fields("id", "name", "address"))
			.contains(row(1000000L, "Bob", "123 Bob St"),
					  row(1000001L, "Sue", "123 Sue St"));
		
		Person bob = personDao.findById(1000000L);
		Person sue = personDao.findById(1000001L);
		
		assertNotNull(bob);
		assertTrue("Bob".equals(bob.getName()));
		assertTrue("123 Bob St".equals(bob.getAddress()));
		
		assertNotNull(sue);
		assertTrue("Sue".equals(sue.getName()));
		assertTrue("123 Sue St".equals(sue.getAddress()));
	}
	
	@Test
	public void shouldBeAbleToUseLargeDataset() {
		RowOfValues[] rows = new RowOfValues[5000];
		for (int i=0; i < rows.length; i++) {
			rows[i] = row(i, "Sue", "123 Sue St");
		}
		
		givenThat(testDB).table("person", fields("id", "name", "address"))
			.containsOnly(rows);
		
		Person firstSue = personDao.findById(0L);
		assertNotNull(firstSue);
		
		Person lastSue = personDao.findById((long)(rows.length - 1));
		assertNotNull(lastSue);
	}
	
	@Test
	public void isEmptyShouldDeleteAllRows() {
		givenThat(testDB).table("person", fields("id", "name"))
			.contains(row(1234567L, "Bob"));
		
		givenThat(testDB).table("person", fields())
			.isEmptyTable();
		
		assertNull(personDao.findById(1234567L));
	}
	
	@Test
	public void containsOnlyShouldNotContainAnyPriorEntries() {
		givenThat(testDB).table("person", fields("id", "name"))
			.contains(row(5000L, "Bob"));
	
		givenThat(testDB).table("person", fields("id", "name"))
			.containsOnly(row(6000L, "bill"));
		
		assertNull(personDao.findById(5000L));
		assertNotNull(personDao.findById(6000L));
	}
	
	@Test (expected=RowFieldsMismatchException.class)
	public void containsShouldThrowExceptionWhenARowDoesntMatchTheFields() {
		givenThat(testDB).table("person", fields("id", "name"))
			.containsOnly(row(6000L, "bill", "extra", "fields", "here"));
	}

}
