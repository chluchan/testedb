package testedb.jdbc;

import static org.junit.Assert.*;
import static testedb.API.*;

import org.junit.Test;

public class RowOfValuesTest {

	@Test
	public void shouldBeEqualWithSameValues() {
		assertTrue(row(1, "2", "3").equals(row(1, "2", "3")));
		assertFalse(row(1, "2", "3").equals(row(1, "2", "4")));
		assertFalse(row(1, "2", "3").equals(row(1, "2", "3", "4")));
		assertFalse(row(1, "2", "3").equals(row("1", "2", "3")));
	}

}
