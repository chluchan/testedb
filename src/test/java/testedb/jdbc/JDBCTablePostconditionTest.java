package testedb.jdbc;

import static org.junit.Assert.*;
import static testedb.API.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import sample.model.PersonDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:**model-config.xml")
@TransactionConfiguration
@Transactional
public class JDBCTablePostconditionTest {
	
	@Autowired
	private PersonDAO personDao;
	
	@Autowired
	private JDBCTestDB testDB;

	@Test
	public void shouldCreatePostconditionVerifier() {
		testedb.PostconditionVerifier<RowOfValues> verifier =
			assertThat(testDB).table("person", fields("id", "name", "address"));
	}
	
	@Test
	public void containsShouldPassWhenRequiredItemsAreFound() {
		givenThat(testDB).table("person", fields("id", "name", "address"))
			.containsOnly(row(1000L, "john doe", "501 doe lane"),
					      row(1001L, "jane doe", "502 doe lane"));
		
		assertThat(testDB).table("person", fields("id", "name", "address"))
			.contains(row(1000L, "john doe", "501 doe lane"));
	}
	
	@Test(expected = AssertionError.class)
	public void containsShouldFailWhenRequiredItemsAreNotFound() {
		givenThat(testDB).table("person", fields("id", "name", "address"))
			.containsOnly(row(1000L, "john doe", "501 doe lane"),
				          row(1001L, "jane doe", "502 doe lane"));
		
		assertThat(testDB).table("person", fields("id", "name", "address"))
			.contains(row(1000L, "john doe", "501 doe lane"),
					  row(2000L, "bob",      "305 bob st"));	
	}
	
	@Test
	public void doesNotContainShouldPassWhenItemDoesntExist() {
		givenThat(testDB).table("person", fields("id", "name", "address"))
			.containsOnly(row(1000L, "john doe", "501 doe lane"),
			              row(1001L, "jane doe", "502 doe lane"));
		
		assertThat(testDB).table("person", fields("id", "name", "address"))
			.doesNotContain(row(1003L, "bill", "801 bill ave"));
	}
	
	@Test(expected = AssertionError.class)
	public void doesNotContainShouldFailWhenItemIsFound() {
		givenThat(testDB).table("person", fields("id", "name", "address"))
			.containsOnly(row(1000L, "john doe", "501 doe lane"),
		                  row(1001L, "jane doe", "502 doe lane"));
	
		assertThat(testDB).table("person", fields("id", "name", "address"))
			.doesNotContain(row(1000L, "john doe", "501 doe lane"));	
	}
	
	@Test
	public void containsOnlyPassesWithExactMatch() {
		givenThat(testDB).table("person", fields("id", "name", "address"))
			.containsOnly(row(1001L, "jane doe", "502 doe lane"),
					      row(1000L, "john doe", "501 doe lane"));
	
		assertThat(testDB).table("person", fields("id", "name", "address"))
			.containsOnly(row(1000L, "john doe", "501 doe lane"),
					      row(1001L, "jane doe", "502 doe lane"));
	}
	
	@Test(expected = AssertionError.class)
	public void containsOnlyFailsWithExtraItem() {
		givenThat(testDB).table("person", fields("id", "name", "address"))
			.containsOnly(row(1000L, "john doe", "501 doe lane"),
					      row(1001L, "jane doe", "502 doe lane"));
		
		assertThat(testDB).table("person", fields("id", "name", "address"))
			.containsOnly(row(1000L, "john doe", "501 doe lane"));
	}
	
	@Test(expected = AssertionError.class)
	public void containsOnlyFailsWithMissingItem() {
		givenThat(testDB).table("person", fields("id", "name", "address"))
			.containsOnly(row(1000L, "john doe", "501 doe lane"));
		
		assertThat(testDB).table("person", fields("id", "name", "address"))
			.containsOnly(row(1000L, "john doe", "501 doe lane"),
					      row(1001L, "jane doe", "502 doe lane"));
	}

}
