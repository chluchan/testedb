package sample.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="person")
public class Person implements Serializable {
	private static final long serialVersionUID = -3739314522644574090L;
	
	private Long id;
	private String name;
	private String address;
	private Long phoneNumber;
	
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	@Column(name = "name", nullable = false)
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name = "address", nullable = true)
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}
	
	@Column(name = "phone_number", nullable = true)
	public Long getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(Long phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null || obj.getClass() != this.getClass()) {
			return false;
		}
		
		Person otherPerson = (Person)obj;
		return (name.equals(otherPerson.getName()) &&
			phoneNumber.equals(otherPerson.getPhoneNumber()) &&
			address.equals(otherPerson.getAddress()));
	}
	
	@Override
	public String toString() {
		return String.format("Person{ id: %d, name: %s, address: %s, phoneNumber: %d }", id, name, address, phoneNumber);
	}
}
