package sample.model.test;

import static org.junit.Assert.*;
import static testedb.API.*;
import static java.util.Arrays.asList;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import sample.domain.Person;
import sample.model.PersonDAO;
import testedb.hibernate.HibernateTestDB;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:**model-config.xml")
@TransactionConfiguration
@Transactional
public class PersonDAOTest {
	
	@Autowired
	private HibernateTestDB testDB;
	
	@Autowired
	private PersonDAO personDao;
	
	@Test
	public void shouldRetrieveAllPeople() {
		givenThat(testDB).table(Person.class)
			.containsOnly(joeBob(), johnDoe(), mikeSmith());
		
		List<Person> allPeople = personDao.allPeople();
		
		assertEquals(3, allPeople.size());
		assertTrue(allPeople.containsAll(asList(joeBob(), johnDoe(), mikeSmith())));
	}
	
	@Test
	public void shouldRetrieveAPersonById() {
		Person joeBob = joeBob();
		givenThat(testDB).table(Person.class)
			.contains(joeBob);
		
		assertEquals(joeBob, personDao.findById(joeBob.getId()));
	}
	
	@Test
	public void shouldReturnNullForInvalidId() {
		Person joeBob = joeBob();
		givenThat(testDB).table(Person.class)
			.containsOnly(joeBob);
		
		assertNull(personDao.findById(joeBob.getId() + 1));
	}
	
	@Test
	public void shouldRetrievePeopleWithName() {
		givenThat(testDB).table(Person.class)
			.containsOnly(joeBob(), johnDoe(), mikeSmith(), joeBobWithDifferentAddress());
		
		List<Person> joeBobs = personDao.findByName("Joe Bob");
		
		assertEquals(2, joeBobs.size());
		assertTrue(joeBobs.containsAll(asList(joeBob(), joeBobWithDifferentAddress())));
	}
	
	@Test
	public void shouldReturnEmptyListIfNameIsNotFound() {
		givenThat(testDB).table(Person.class)
			.containsOnly(joeBob(), johnDoe(), mikeSmith());
	
		List<Person> karens = personDao.findByName("Karen");
		
		assertTrue(karens.isEmpty());
	}
	
	@Test
	public void shouldCreateAListOfPeople() {
		givenThat(testDB).table(Person.class)
			.isEmptyTable();
		
		Person joeBob = joeBob();
		Person johnDoe = johnDoe();
		List<Person> newPeople = personDao.persist(joeBob, johnDoe);
		
		assertThat(testDB).table(Person.class)
			.contains(joeBob, johnDoe);
		assertTrue(newPeople.containsAll(asList(joeBob, johnDoe)));
	}
	
	@Test
	public void shouldUpdateAnExistingPerson() {
		Person joeBob = joeBob();
		givenThat(testDB).table(Person.class)
			.containsOnly(joeBob);
		
		String newAddress = "1076 S Orange Blvd";
		joeBob.setAddress(newAddress);
		List<Person> updatedPeople = personDao.persist(joeBob);
		
		assertTrue(updatedPeople.contains(joeBob));
		assertTrue(updatedPeople.get(0).getAddress().equals(newAddress));
		assertThat(testDB).table(Person.class)
			.containsOnly(updatedPeople.get(0));
	}

	private static final Person joeBob() {
		Person person = new Person();
		person.setName("Joe Bob");
		person.setPhoneNumber(2569375148L);
		person.setAddress("642 Elm Ave");
		return person;
	}
	
	private static final Person joeBobWithDifferentAddress() {
		Person person = new Person();
		person.setName("Joe Bob");
		person.setPhoneNumber(2569945748L);
		person.setAddress("1353 Palm Dr");
		return person;
	}
	
	private static final Person johnDoe() {
		Person person = new Person();
		person.setName("John Doe");
		person.setPhoneNumber(2523421438L);
		person.setAddress("642 Elm Ave");
		return person;
	}
	
	private static final Person mikeSmith() {
		Person person = new Person();
		person.setName("mikeSmith");
		person.setPhoneNumber(2523246438L);
		person.setAddress("578 Elm Ave");
		return person;
	}
}
