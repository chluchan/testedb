package sample.model;

import static java.util.Arrays.asList;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import sample.domain.Person;

public class PersonDAO {
	private final SessionFactory sessionFactory;
	
	@Autowired
	public PersonDAO(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	
	@SuppressWarnings("unchecked")
	public List<Person> allPeople() {
		Session session = sessionFactory.getCurrentSession();
		
		String sql = new StringBuffer()
			.append(" SELECT person")
			.append(" FROM ").append(Person.class.getName()).append(" as person")
			.toString();
		
		Query query = session.createQuery(sql);
		return query.list();
	}
	
	public Person findById(final Long id) {
		Session session = sessionFactory.getCurrentSession();
		
		String sql = new StringBuffer()
			.append(" SELECT person")
			.append(" FROM ").append(Person.class.getName()).append(" as person")
			.append(" WHERE person.id=:id ")
			.toString();
		
		Query query = session.createQuery(sql);
		query.setLong("id", id);
		
		@SuppressWarnings("unchecked")
		List<Person> people =  query.list();
		
		if (people.isEmpty()) {
			// TODO Option.None would be more appropriate
			return null;
		} else {
			return people.get(0);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Person> findByName(final String name) {
		Session session = sessionFactory.getCurrentSession();
				
		String sql = new StringBuffer()
			.append(" SELECT person")
			.append(" FROM ").append(Person.class.getName()).append(" as person")
			.append(" WHERE person.name=:name ")
			.toString();
		
		Query query = session.createQuery(sql);
		query.setString("name", name);
		
		return query.list();
	}
	
	@Transactional
	public List<Person> persist(Person... newPeople) {
		Session session = sessionFactory.getCurrentSession();
		
		for (Person person : newPeople) {
			session.saveOrUpdate(person);
		}
		
		return asList(newPeople);
	}
	
}
